package com.infogain.igtrainingfeedback.igfeedbackform;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

public class MainActivity extends AppCompatActivity {

    private RecyclerView mTrainingList;
    private TrainingRecycleAdapter adapter;
    LinearLayoutManager layoutManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        getSupportActionBar().setTitle("Training Feedback");

        mTrainingList = (RecyclerView) findViewById(R.id.trainingList);
        adapter =new TrainingRecycleAdapter(this);
        layoutManager=new LinearLayoutManager(this);

        mTrainingList.setAdapter(adapter);
        mTrainingList.setLayoutManager(layoutManager);




    }
}
