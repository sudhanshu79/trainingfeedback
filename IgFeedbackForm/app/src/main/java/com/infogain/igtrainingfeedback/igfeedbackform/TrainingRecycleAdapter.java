package com.infogain.igtrainingfeedback.igfeedbackform;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

/**
 * Created by Sudhanshu.Sharma on 16-11-2016.
 */
public class TrainingRecycleAdapter extends RecyclerView.Adapter<TrainingRecycleAdapter.MyViewHolder> {


    Context ctx;

    public TrainingRecycleAdapter(Context ctx)
    {
        this.ctx=ctx;
    }
    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
       View rootView=LayoutInflater.from(ctx).inflate(R.layout.training_listitem_layout,parent,false);
        MyViewHolder holder=new MyViewHolder(rootView);
        return holder;
    }

    @Override
    public int getItemCount() {
        return 10;
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ctx.startActivity(new Intent(ctx,RatingActivity.class));
            }
        });

    }

    public class MyViewHolder extends RecyclerView.ViewHolder
    {

        public MyViewHolder(View itemView) {
            super(itemView);

            TextView mTxtDate=(TextView)itemView.findViewById(R.id.training_txt_date);
            TextView mTxtName=(TextView)itemView.findViewById(R.id.training_txt_name);
            TextView mTxtTrainer=(TextView)itemView.findViewById(R.id.training_txt_trainer);
        }
    }
}
