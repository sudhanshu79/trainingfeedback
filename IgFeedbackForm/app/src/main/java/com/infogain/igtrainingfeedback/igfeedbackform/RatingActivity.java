package com.infogain.igtrainingfeedback.igfeedbackform;

import android.app.Dialog;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.LinearLayout;

public class RatingActivity extends AppCompatActivity {

    private  LinearLayout cmntLayout;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rating);

        getSupportActionBar().setTitle("Rating");
        cmntLayout=(LinearLayout)findViewById(R.id.leavecmnt_layout);

        cmntLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                new LeaveCommentDialog().show(getSupportFragmentManager(),"Comment");
            }
        });


    }

    public static class LeaveCommentDialog extends DialogFragment{

        @Override
        public void onCreate(@Nullable Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            setStyle(STYLE_NO_FRAME, android.R.style.Theme_Holo_Light);


        }

        @Nullable
        @Override
        public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

            View rootView=inflater.inflate(R.layout.leavecmnt,container,false);
            rootView.findViewById(R.id.cancel_dialog).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    getDialog().dismiss();
                }
            });
            return rootView;
        }

        @Override
        public void onActivityCreated(Bundle savedInstanceState) {
            super.onActivityCreated(savedInstanceState);
            getDialog().getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT,
                    WindowManager.LayoutParams.MATCH_PARENT);

            getDialog().getWindow()
                    .getAttributes().windowAnimations = R.style.MyCustomTheme;

        }
    }


}
